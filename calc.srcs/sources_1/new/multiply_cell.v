`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/11/30 16:05:23
// Design Name: 
// Module Name: multiply_cell
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 这个比较反人类，需要缓存多个上一个状态（被乘数，乘数，累加数，状态值），换一个正常点的思路
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module multiply_cell #(
    parameter N = 4,
    parameter M = 4
) (
    input clk,
    input rstn,  // 是否重置 0重置，1恢复
    input [N-1:0] multn,  // 输入的被乘数
    input [M-1:0] multm,  // 输入的乘数
    input [M-1:0] index,  // 乘法器的索引值
    input en,  // 当前输入数据是否准备好
    input [M+N-1:0] mult_in,  // 输入的累加数
    output reg rdy,  // 当前输出数据是否准备好
    output reg [M+N-1:0] mult_out,  // 输出的累加数
    output reg [N-1:0] multn_current, // 当前缓存的被乘数，用于输入到下一个索引的额乘法器
    output reg [M-1:0] multm_current  // 当前缓存的乘数，用于输入到下一个索引的额乘法器
);
  always @(posedge clk or negedge rstn) begin
    if (!rstn || !en) begin
      rdy <= 0;
      mult_out <= 0;
      multn_current <= 0;
      multm_current <= 0;
    end else if (multm[index]) begin // 运算完成后将数据输出到下一个索引的计算器
      rdy <= 1;
      mult_out <= mult_in + ({{(M) {1'b0}}, multn} << index);  // 补充位数并左移
      multn_current <= multn;
      multm_current <= multm;
    end else begin
      rdy <= 1;
      mult_out <= mult_in;
      multn_current <= multn;
      multm_current <= multm;
    end
  end
endmodule
