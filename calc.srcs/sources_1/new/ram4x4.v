`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/11/29 10:21:21
// Design Name: 
// Module Name: ram4x4
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 4bit地址长度 4bit数据长度的 缓存器
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ram4x4 (
    input clk,
    input enable,
    input writeable,
    input [3:0] addr,
    input [3:0] data,
    output reg [3:0] sout
);
  parameter MASK = 15;
  reg [3:0] mem[0:15];
  always @(posedge clk) begin
    if (enable && writeable) begin
      mem[addr] <= data & MASK;
    end else if (enable && !writeable) begin
      sout <= mem[addr] & MASK;
    end
  end
endmodule
