`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/11/27 16:04:03
// Design Name: 
// Module Name: full_addr
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module full_addr (
    input  Ai,
    input  Bi,
    input  Ci,
    output So,
    output Co
);
  assign {Co, So} = Ai + Bi + Ci;
endmodule
