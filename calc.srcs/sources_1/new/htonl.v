`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/11/29 13:28:57
// Design Name: 
// Module Name: htonl
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module htonl #(
    parameter N = 2
) (
    input enable,
    input [N-1:0] sin,
    output [N-1:0] sout
);

  reg [N-1:0] out_tmp;
  always @(*) begin
    if (enable) begin
      out_tmp = rev(sin);
    end else begin
      out_tmp = 0;
    end
  end

  function [N-1:0] rev;
    input [N-1:0] din;
    integer i;
    for (i = 0; i < N; i = i + 1) begin
      rev[N-1-i] = din[i];
    end

  endfunction

  assign sout = out_tmp;

endmodule
