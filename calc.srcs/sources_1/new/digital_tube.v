`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/11/29 14:50:22
// Design Name: 
// Module Name: digital_tube
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module digital_tube (
    input clk,
    input enable,
    input reset,
    input [3:0] d1,
    input [3:0] d2,
    input [3:0] d3,
    input [3:0] d4,
    output reg [1:0] index,
    output reg [6:0] sout
);
  reg [1:0] select;
  initial begin
    select = 0;
  end

  always @(posedge clk or negedge reset) begin
    if (!reset) begin
      index <= 0;
      sout  <= 0;
    end else if (enable) begin
      index <= select;
      case (select)
        0: sout <= translate_number(.number(d1));
        1: sout <= translate_number(.number(d2));
        2: sout <= translate_number(.number(d3));
        3: sout <= translate_number(.number(d4));
        default: sout <= translate_number(.number(d1));
      endcase
      select <= select + 1;  // 移动到下一位
    end
  end

  function [6:0] translate_number;
    input [3:0] number;
    case (number)
      0: translate_number = 7'b1111110;
      1: translate_number = 7'b0110000;
      2: translate_number = 7'b1101101;
      3: translate_number = 7'b1111001;
      4: translate_number = 7'b0110011;
      5: translate_number = 7'b1011011;
      6: translate_number = 7'b1011111;
      7: translate_number = 7'b1110000;
      8: translate_number = 7'b1111111;
      9: translate_number = 7'b1111011;
      default: translate_number = 7'b1111110;
    endcase

  endfunction

endmodule
