`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/11/28 17:04:45
// Design Name: 
// Module Name: full_add4
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 4bit全加器
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module full_add4 (
    input [3:0] Ai,
    input [3:0] Bi,
    input Ci,
    output [4:0] So
);
  wire [3:0] Co_tmp;
  genvar i;
  generate
    for (i = 0; i < 4; i = i + 1) begin
      full_addr u_full_addr (
          .Ai(Ai[i]),
          .Bi(Bi[i]),
          .Ci(i == 0 ? Ci : Co_tmp[i-1]),
          .So(So[i]),
          .Co(Co_tmp[i])
      );
    end
  endgenerate
  assign So[4] = Co_tmp[3];
endmodule
