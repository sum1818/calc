`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/11/28 10:16:15
// Design Name: 
// Module Name: mux4to1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mux4to1 (
    input  [1:0] sel,
    input  [1:0] p1,
    input  [1:0] p2,
    input  [1:0] p3,
    input  [1:0] p4,
    output [1:0] out
);
  reg [1:0] out_t;
  always @(*) begin
    case (sel)
      2'b00:   sout_t = p1;
      2'b01:   sout_t = p2;
      2'b10:   sout_t = p3;
      default: sout_t = p4;
    endcase
  end
  assign out = out_t;
endmodule
