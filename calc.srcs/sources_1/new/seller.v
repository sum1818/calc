`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/11/29 18:26:34
// Design Name: 
// Module Name: seller
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 自动售货机
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module seller (
    input clk,
    input rstn,  // 复位
    input [1:0] coin,  // 投币金额 0：0元(未投/复位) 1: 0.5元 2：1元 3：错误
    output state,  // 输出当前状态 0:投币中 1:售出
    output [2:0] total  // 输出当前金额 0.5 * total元
);

  reg [2:0] total_temp;  // 当前余额
  reg state_temp;  // 当前状态

  initial begin
    state_temp = 0;
    total_temp = 0;
  end

  always @(posedge clk or negedge rstn) begin
    if (!rstn) begin
      state_temp <= 0;
      total_temp <= 0;
    end
  end
  //根据投币情况来确定售货机状态, 敏感信号填写clk和coin(这里用@(*)会不触发 代码块内没用到可能不触发) 避免产生锁存器
  always @(posedge clk or negedge rstn or coin) begin 
    if ((total_temp + coin) >= 4) begin  // 当前余额超过2元 0.5*4
      state_temp <= 1;  // 标记状态已售出
      total_temp <= total_temp + coin - 4;  // 当前余额减掉 商品金额
    end else begin
      state_temp <= 0;  // 标记状态投币中
      total_temp <= total_temp + coin;
    end
  end

  assign state = state_temp;
  assign total = total_temp;


endmodule
