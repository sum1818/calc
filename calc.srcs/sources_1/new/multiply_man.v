`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/12/01 13:25:42
// Design Name: 
// Module Name: multiply_man
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 流水线乘法器
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module multiply_man #(
    parameter N = 4,
    parameter M = 4
) (
    input clk,
    input rstn,
    input rdy,  // 输入数据是否准备好
    input [N-1:0] multn,  // 被乘数
    input [M-1:0] multm,  // 乘数
    output finish,  // 是否完成计算结果
    output [N+M-1:0] out  //计算结果
);

  integer index;  // 当前正在工作的乘法器

  wire [N-1:0] all_multn[M-1:0];  // 所有被乘数
  wire [M-1:0] all_multm[M-1:0];  // 所有乘数
  wire all_rdy[M-1:0];  // 所有完成结果
  wire [N+M-1:0] all_out[M-1:0];  // 所有计算结果

  genvar i;

  generate
    for (i = 0; i < M; i = i + 1) begin : toob
      integer t = i;
      multiply_cell #(
          .M(M),
          .N(N)
      ) u_multiply_cell (
          .clk(clk),
          .rstn(rstn),
          .multn(i == 0 ? multn : all_multn[i-1]),
          .multm(i == 0 ? multm : all_multm[i-1]),
          .index(i),
          .en(i == 0 ? rdy: all_rdy[i-1]), // 上一个乘法器数据计算完成后才能开始计算
          .mult_in(i == 0 ? 0: all_out[i-1]), // 上一个乘法器数据累加完成后作为下一个乘法器的输入累加
          .rdy(all_rdy[i]),  // 当前乘法器的计算结果
          .mult_out(all_out[i]),  // 当前乘法器的累加结果
          .multn_current(all_multn[i]), // 当前乘法器缓存的被乘数 用于向下一个乘法器输入
          .multm_current(all_multm[i]) // 当前乘法器缓存的乘数 用于向下一个乘法器输入
      );
    end
  endgenerate

  assign finish = all_rdy[M-1];  // 绑定输出端口
  assign out = all_out[M-1];  // 绑定输出端口


endmodule
