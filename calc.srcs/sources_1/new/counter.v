`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/11/28 14:41:50
// Design Name: 
// Module Name: counter
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module counter (
    input rstn,
    input clk,
    output [3:0] cnt,
    output cout
);

  reg [3:0] cnt_tmp;
  always @(posedge clk or negedge rstn) begin
    if (!rstn) begin
      cnt_tmp <= 0;
    end else if (cnt_tmp == 4'd9) begin
      cnt_tmp <= 0;
    end else begin
      cnt_tmp <= cnt_tmp + 1'b1;
    end
  end
  assign cnt  = cnt_tmp;
  assign cout = (cnt_tmp == 4'd9);
endmodule
