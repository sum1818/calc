`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/11/29 18:26:52
// Design Name: 
// Module Name: test11
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test11 ();

  reg clk;
  reg rstn;
  reg [1:0] coin;
  wire state;
  wire [2:0] total;
  reg [9:0]    buy_oper ;

  initial begin
    clk      = 0;
    buy_oper = 'h0;
    coin     = 2'h0;
    rstn     = 1'b1;
    buy_oper = 10'b00_0101_0101;
    repeat (5) begin
      @(negedge clk);
      coin     = buy_oper[1:0];
      buy_oper = buy_oper >> 2;
    end
    #16;
    buy_oper = 10'b00_0010_0110;
    repeat (5) begin
      @(negedge clk);
      coin     = buy_oper[1:0];
      buy_oper = buy_oper >> 2;
    end
  end
  //   seller u_seller (
  //       .clk  (clk),
  //       .rstn (rstn),
  //       .coin (coin),
  //       .state(state),
  //       .total(total)
  //   );

  sellers u_seller (
      .clk  (clk),
      .rstn (rstn),
      .coin (coin),
      .state(state),
      .total(total)
  );

  always begin
    #10;
    clk = ~clk;
    if ($time >= 1000) begin
      $finish;
    end
  end

endmodule
