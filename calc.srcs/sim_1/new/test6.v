`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/11/29 10:22:00
// Design Name: 
// Module Name: test6
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test6 ();

  reg clk;
  reg enable;
  reg writeable;
  reg [3:0] addr;  // 内存地址
  reg [3:0] data;  // 写入数据
  wire [3:0] sout;  // 读取输出

  initial begin
    clk = 0;
    addr = 0;
    data = 0;
    enable = 1;
    writeable = 1;
    // 开始写入mem
    repeat (10) begin
      // 等待时钟变换再执行操作
      @(negedge clk);
      addr = addr + 1;
      data = data + 1;
    end
    addr = 0;
    writeable = 0;
    repeat (10) begin
      @(negedge clk);
      addr = addr + 1;
    end
  end

  defparam u_ram4x4.MASK = 7;
  ram4x4 u_ram4x4 (
      .clk(clk),
      .enable(enable),
      .writeable(writeable),
      .addr(addr),
      .data(data),
      .sout(sout)
  );

  always begin
    #10 clk = ~clk;
    if ($time >= 1000) begin
      $finish;
    end
  end
endmodule
