`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/11/30 16:05:44
// Design Name: 
// Module Name: test12
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test12 ();

  parameter N = 8;
  parameter M = 4;
  reg clk;
  reg rstn;
  reg [N-1:0] multn;
  reg [M-1:0] multm;
  wire finish;
  wire [N+M-1:0] out;

  initial begin
    clk  = 0;
    rstn = 0; // 初始状态设置为重置 免得直接开始计算
    mult_task(.mult1(8'd12), .mult2(4'd3));
    $display("res:%d", out);
    mult_task(.mult1(8'd13), .mult2(4'd5));
    $display("res:%d", out);
    mult_task(.mult1(8'd15), .mult2(4'd3));
    $display("res:%d", out);

  end

  task mult_task;
    input [N+M-1:0] mult1;
    input [N+M-1:0] mult2;
    begin
      wait (!test12.u_multiply.finish); // 重置rstn需要等待下一个周期
      multn = mult1;
      multm = mult2;
      @(negedge clk);
      rstn = 1;  // 等待一个时钟开始计算，避免数据冲突
      wait (test12.u_multiply.finish); // 等待计算结果
      @(negedge clk); // 等待一个时钟周期 显示计算结果
      rstn = 0;
    end
  endtask

  multiply #(
      .N(N),
      .M(M)
  ) u_multiply (
      .clk(clk),
      .rstn(rstn),
      .multn(multn),
      .multm(multm),
      .finish(finish),
      .out(out)
  );

  always begin
    #10;
    clk = ~clk;
    if ($time >= 1000) begin
      $finish;
    end
  end
endmodule
