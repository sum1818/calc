`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/11/28 17:31:06
// Design Name: 
// Module Name: test5
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 4位全加器测试
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test5(

    );

    reg [3:0] Ai;
    reg [3:0] Bi;
    wire [4:0] So;
    initial begin
        Ai = 4'd5;
        Bi = 4'd2;
        #10;
        Ai = 4'd10;
        Bi = 4'd8;
    end

full_add4 u_full_add4(
    .Ai(Ai),
    .Bi(Bi),
    .Ci(1'b0),
    .So(So)
);

always begin
    #100;
    if ($time >= 1000) begin
      $finish;
    end
  end

endmodule
