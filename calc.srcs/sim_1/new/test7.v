`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/11/29 13:29:17
// Design Name: 
// Module Name: test7
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test7(

    );

    reg enable;
    reg [3:0] sin;
    wire [3:0] sout;

    initial begin
        enable = 1;
        sin = 4'b1100;
    end

    htonl #(.N(4)) u_htol(
        .enable(enable),
        .sin(sin),
        .sout(sout)
    );
endmodule
