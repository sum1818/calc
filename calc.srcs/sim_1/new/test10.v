`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/11/29 15:21:58
// Design Name: 
// Module Name: test10
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test10 ();

  reg clk;
  reg enable;
  reg reset;
  reg [3:0] data[0:3];
  wire [1:0] index;
  wire [6:0] sout;

  initial begin
    clk = 0;
    enable = 1;
    reset = 1;
    data[0] = 6;
    data[1] = 3;
    data[2] = 7;
    data[3] = 8;
    forever begin
      #10;
      clk = ~clk;
    end
  end

  digital_tube u_digital_tube (
      .clk(clk),
      .enable(enable),
      .reset(reset),
      .d1(data[0]),
      .d2(data[1]),
      .d3(data[2]),
      .d4(data[3]),
      .index(index),
      .sout(sout)
  );

  always begin
    #100;
    if ($time >= 1000) begin
      $finish;
    end
  end

endmodule
