`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/12/06 16:13:18
// Design Name: 
// Module Name: test15
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test15 ();

  parameter N = 5;
  parameter M = 3;

  reg clk;
  reg rstn;
  reg [N-1:0] dividend;  //被除数
  reg [M-1:0] divisor;  //除数
  reg data_rdy;

  wire res_rdy;
  wire [N-1:0] merchant;  //商位宽：N
  wire [N-1:0] remainder;

  initial begin
    clk  = 0;
    rstn = 0;
    #10;
    dividend = 16;
    divisor = 3;
    rstn = 1;
  end

  divider #(
      .M(M),
      .N(N)
  ) m_divider (
      .clk(clk),
      .rstn(rstn),
      .dividend(dividend),
      .divisor(divisor),
      .data_rdy(data_rdy),
      .res_rdy(res_rdy),
      .merchant(merchant),
      .remainder(remainder)
  );

  always begin
    #10;
    clk = ~clk;
    if ($time >= 1000) begin
      $finish;
    end
  end
endmodule
