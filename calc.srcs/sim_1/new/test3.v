`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/11/28 14:49:01
// Design Name: 
// Module Name: test3
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test3 ();
  reg clk;
  reg rstn;
  wire cout;
  wire [3:0] cnt;
  initial begin
    clk  = 1'b0;
    rstn = 1'b0;
    #10 rstn = 1'b1;
  end
  counter m_counter (
      .clk (clk),
      .rstn(rstn),
      .cout(cout),
      .cnt (cnt)
  );
  always begin
    #10;
    clk = ~clk;
    if ($time >= 1000) begin
      $finish;
    end
  end
endmodule
