`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/12/01 13:26:05
// Design Name: 
// Module Name: test13
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test13 ();

  parameter N = 8;
  parameter M = 4;

  reg clk;
  reg rstn;
  reg [N-1:0] multn;
  reg [M-1:0] multm;
  reg rdy;
  wire finish;
  wire [N+M-1:0] out;

  initial begin
    clk  = 0;
    rstn = 1;
    rdy = 1;
    @(negedge clk);
    mult_task(.mult1(8'd12), .mult2(4'd3));
    mult_task(.mult1(8'd12), .mult2(4'd4));
    mult_task(.mult1(8'd12), .mult2(4'd5));
    mult_task(.mult1(8'd12), .mult2(4'd6));
  end

  task mult_task;
    input [N+M-1:0] mult1;
    input [N+M-1:0] mult2;
    begin
      multn = mult1;
      multm = mult2;
      @(negedge clk);
    end
  endtask

  multiply_man #(
      .M(M),
      .N(N)
  ) u_multiply_man (
      .clk(clk),
      .rstn(rstn),
      .rdy(1),
      .multn(multn),
      .multm(multm),
      .finish(finish),
      .out(out)
  );

  always begin
    #10;
    clk = ~clk;
    if ($time >= 1000) begin
      $finish;
    end
  end

endmodule
