`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/12/06 15:29:58
// Design Name: 
// Module Name: test30
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test30(

    );

    reg [7:0] aot;
    reg [7:0] aot2;
    initial begin
        aot = 7'b1001001;
        aot2 = {aot[6:4], 1'b0};
    end
endmodule
