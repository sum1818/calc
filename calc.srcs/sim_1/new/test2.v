`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/11/28 10:46:39
// Design Name: 
// Module Name: test2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test2 ();

  reg  [1:0] sel;
  wire [1:0] sout;

  mux4to1 u_mux4to1 (
      .sel(sel),
      .p1 (2'b00),
      .p2 (2'b01),
      .p3 (2'b10),
      .p4 (2'b11),
      .out(sout)
  );

  initial begin
    sel = 2'b00;
    #10 sel = 3;
    #10 sel = 1;
    #10 sel = 0;
    #10 sel = 2;
  end

  always begin
    #100;
    //$display("---gyc---%d", $time);
    if ($time >= 1000) begin
      $finish;
    end
  end


endmodule
