`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/11/29 14:20:07
// Design Name: 
// Module Name: test8
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test8 ();
  reg [3:0] a;
  reg [3:0] b;
  reg [3:0] c;

  initial begin
    a = 1;
    b = 2;
    test(.a(a), .b(b), .c(c));
  end

  task test;
    input [3:0] a;
    input [3:0] b;
    output [3:0] c;
    c = a + b;
  endtask
endmodule
