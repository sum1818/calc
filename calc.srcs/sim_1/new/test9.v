`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/11/29 14:32:58
// Design Name: 
// Module Name: test9
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test9 ();
  task automatic test_flag;
    input [3:0] cnti;
    input en;
    output [3:0] cnto;
    if (en) cnto = cnti;
  endtask

  reg       en_cnt;
  reg [3:0] cnt_temp;
  reg       clk;
  initial begin
    en_cnt = 1;
    cnt_temp = 0;
    clk = 0;
    #25;
    en_cnt = 0;
  end
  always begin
    #10;
    clk = ~clk;
    cnt_temp = cnt_temp + 1;
  end

  reg [3:0] cnt1, cnt2;
  always @(posedge clk) test_flag(2, en_cnt, cnt1);  //task(1)
  always @(posedge clk) test_flag(cnt_temp, !en_cnt, cnt2);  //task(2)
endmodule
